import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

// export default new Vuex.Store({
//   state: {},
//   mutations: {},
//   actions: {}
// });



export default new Vuex.Store({
    state: {
        token:'',
        isAuthenticated: false
    },
    mutations:{
        initializeStore(state){
            if ( localStorage.getItem('token')){
                console.log("s")
                state.token=localStorage.getItem('token')
                state.isAuthenticated=true
            }else{
                state.token=''
                state.isAuthenticated=false
            }
            },
            setToken(state,token){
                console.log("SSSSSSS")
                state.token=token
                state.isAuthenticated=true
            },
            removeToken(state){
                console.log("SSSSSSS")
                state.token=''
                state.isAuthenticated=false
            }
        },
        actions:{

        },
        modules:{
            
        }
    
})