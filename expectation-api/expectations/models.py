from django.db import models

# Create your models here.
class Expectation(models.Model):
    name = models.CharField(max_length=255)
    expectation = models.TextField()
    when_it_was_created = models.DateTimeField(auto_now_add=True)
    planned_to_finished=models.CharField(max_length=255,null=True,blank=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Archive(models.Model):
    name = models.CharField(max_length=255,null=True,blank=True)
    expectation = models.TextField()
    expectations = models.IntegerField()
    when_it_was_created = models.CharField(max_length=255,null=True,blank=True)
    planned_to_finished=models.CharField(max_length=255,null=True,blank=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

