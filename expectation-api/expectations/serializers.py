from rest_framework import serializers
from .models import Expectation,Archive
from django.contrib.auth.models import User

class ExpectationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Expectation
        fields = ('id', 'name', 'expectation', 'planned_to_finished','when_it_was_created', 'updated_at'
        )
class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username','password'
        )

class ArchiveSerializer(serializers.ModelSerializer):
    class Meta:
        model = Archive
        fields = ('id', 'name', 'expectation',
            'expectations', 'planned_to_finished','when_it_was_created', 'updated_at'
        )