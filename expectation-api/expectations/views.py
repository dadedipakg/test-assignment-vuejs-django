from .models import Expectation,Archive
from .serializers import ExpectationSerializer,UserSerializer,ArchiveSerializer
from rest_framework import generics
from django.contrib.auth.models import User

from rest_framework.views import APIView 
from rest_framework.permissions import AllowAny
import datetime
from datetime import datetime, timedelta
from rest_framework import status, views, permissions

from django.http import JsonResponse
from rest_framework.response import Response

from django.contrib.auth import get_user_model
from rest_framework.authtoken.models import Token
from rest_framework import status, views, permissions
User = get_user_model()
from django.contrib.auth import authenticate, login as auth_login

class ExpectationsList(generics.ListCreateAPIView):
    queryset = Expectation.objects.all()
    serializer_class = ExpectationSerializer

class ExpectationsDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Expectation.objects.all()
   

    serializer_class = ExpectationSerializer
    

class UsersList(generics.ListCreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class ArchiveList(generics.ListCreateAPIView):
    queryset = Archive.objects.all()
    serializer_class = ArchiveSerializer

class ExpectationListView(APIView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request, *args, **kwargs):
        queryset=Expectation.objects.all()
        expectation=Expectation.objects.filter(name=request.data["name"])
        
        for i in expectation:
            Archive.objects.create(name=i.name,
                expectation=i.expectation,
                planned_to_finished=i.planned_to_finished,
                when_it_was_created=i.when_it_was_created
                )
        archive=Archive.objects.all()
        previous_expectation=Expectation.objects.filter(name=request.data["name"])
        previous_expectation.delete()

        expectation=Expectation.objects.create(name=request.data["name"],
            expectation=request.data["expectation"],
            planned_to_finished=request.data["planned_to_finished"],
            )
        expectations=Expectation.objects.get(id=expectation.id)
        return Response({'when_it_was_created':expectations.when_it_was_created},
                    status=status.HTTP_201_CREATED)

def authenticate(self, request, username=None, password=None, **kwargs):
        try:
        
            user = User.objects.get(username=username)
            print(user)
            user.set_password(password)
            user.save()
            return user
            
        except User.DoesNotExist:
            return None

        if user.check_password(password):
            user.is_active = True
            
            user.set_password(password)
            user.save()
            return user

class LoginView(APIView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request, *args, **kwargs):

        username = request.data['username']
        password = request.data['password']
        
        user = authenticate(self,request,username=username, password=password)
        try:
            token = Token.objects.get(user=user)
        except:
            token = Token.objects.create(user=user)
        if user is not None:
            return Response({'token': token.key,'username':username},
                    status=status.HTTP_201_CREATED)

        else:
            return Response("Success", status=404)

    