from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from expectations import views

urlpatterns = [
    path('expectations/', views.ExpectationsList.as_view()),
    path('archive/', views.ArchiveList.as_view()),
    path('usering/', views.UsersList.as_view()),
    path('expectations/<int:pk>/', views.ExpectationsDetail.as_view()),
    path('user/login/', views.LoginView.as_view()),
    path('expectation/', views.ExpectationListView.as_view()),
    

]
