from django.contrib import admin

# Register your models here.
from .models import Expectation,Archive
admin.site.register(Expectation)
admin.site.register(Archive)